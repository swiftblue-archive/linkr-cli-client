# Linkr CLI Client

## Description

Command line interface client for linkr backend

## Usage

To install linkr from npm, run:

```
$ npm install -g linkr
```

To install linkr from private repo, run:

```
$ npm install git+https://<username>:<password>@bitbucket.org/swiftblue/linkr-cli-client.git
```

```linkr --help```

## License

Copyright (c) 2016 

[MIT License](http://en.wikipedia.org/wiki/MIT_License)

## Acknowledgments

Built using [generator-commader](https://github.com/Hypercubed/generator-commander).
