var _        = require('lodash');
var validate = require( __dirname + '/../lib/validate' );
var config   = require( __dirname + '/../lib/config' );
var output   = require( __dirname + '/../lib/output' );


module.exports = function( url ) {
	if (_.isUndefined( url )) {
		return output.success( 'CURRENT SERVER URL SETTING', config.get( 'server' ) );
	} else {
		return setServerUrl( url );
	}
}

function setServerUrl( url ) {
	var url       = validate.url( url );
	config.set( 'server', url );
	return output.success( 'SERVER URL SAVED', url );
}