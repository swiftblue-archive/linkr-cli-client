var Promise      = require('bluebird');
var restify      = require('restify');
var randomString = require('random-string');
var _            = require('lodash');
var config       = require( __dirname + '/../lib/config' );
var api          = require( __dirname + '/../lib/api' );
var output       = require( __dirname + '/../lib/output' );

var controller = {
	create: function( url, slug ) {	
		var url   = _.trim( url );
		var slug  = _.trim( slug || generateSlug() );

		api.create( url, slug )
			.then(function( response ) {
				var slug = response.payload.slug;
				
				output.success( 'NEW LINK CREATED', linkUrl(slug) );
			})
			.catch(function( error ) {
				throw error;
			});
	},

	delete: function( slug ) {	
		var slug = _.trim( slug );

		api.delete( slug )
			.then(function( response ) {
				var slug = response.payload.slug;
				
				output.success( 'LINK DELETED', linkUrl(slug) );
			})
			.catch(function( error ) {
				throw error;
			});
	},

	restore: function( slug ) {	
		var slug = _.trim( slug );

		api.restore( slug )
			.then(function( response ) {
				var slug = response.payload.slug;
				
				output.success( 'LINK RESTORED', linkUrl(slug) );
			})
			.catch(function( error ) {
				throw error;
			});
	},	

	info: function( slug ) {	
		var slug = _.trim( slug );

		api.info( slug )
			.then(function( response ) {
				
				delete response.payload.views;
				delete response.payload.id;

				output.info( response.payload );
			})
			.catch(function( error ) {
				throw new Error( error.message );
			});
	}
}

module.exports = controller;

function generateSlug() { 
	return randomString({ length: 5, numeric: true, letters: true, special: false }); 
}

function linkUrl( slug ) {	
	return config.get('server') + slug;	
}