var os         = require('os');
var fileExists = require('file-exists');
var touch      = require('touch');
var json       = require('jsonsave');
var _          = require('lodash');

var configFile = os.homedir() + '/.linkr.config';

var config     = {

	data: function() {		
		if (fileExists( configFile ) === false) return createConfigFile();
		return json.new( configFile );
	},

	get: function( key ) {
		var _this = this;
		var data  = _this.data();
		return data[ _.trim(key) ];
	},

	set: function( key, value ) {
		var _this      = this;
		var data       = _this.data();
		var setData    = {};
		setData[ _.trim(key) ] = value;
		
		data.$$merge( setData );
		data.$$save();

		return value;
	}
}

module.exports = config;

function createConfigFile( callback ) {
	var defaults = { server: false };

	touch( configFile );

	var data = json.new( configFile );
	data.$$merge( defaults );
	data.$$save();

	return data;
}