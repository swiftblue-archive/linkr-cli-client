var Promise = require('bluebird');
var request = require('request-json');
var _       = require('lodash');
var config  = require( __dirname + '/config' );
var package = require( __dirname + '/../package' );

var api = {
	create: function( url, slug ) {
		var data = { url: url, slug: slug };
		return new Promise.try(function() { return Client.post( 'create', data ); });
	},

	info: function( slug ) {
		var endpoint = 'details/' + slug;
		return Client.get( endpoint );
	},

	delete: function( slug ) {
		var endpoint = 'delete/' + slug;
		return Client.get( endpoint );
	},

	restore: function( slug ) {
		var endpoint = 'restore/' + slug;
		return Client.get( endpoint );
	}
}

module.exports = api;

var Client = {

	post: function( endpoint, data ) {
		var client = getClient();

		return new Promise(function( resolve, reject ) {
    		client.post( endpoint, data, function( err, res, body ) {
    			if (err) return reject(err);
    			if (body.status == 'error') return reject(body.message || 'An unspecified error occured from the server');
    			return resolve(body);
    		});
		});
	},

	get: function( endpoint, data ) {
		var client = getClient();

		return new Promise(function( resolve, reject ) {
    		client.get( endpoint, data, function( err, res, body ) {
    			if (err) return reject(err);
    			if (body.status == 'error') return reject(body.message || 'An unspecified error occured from the server');
    			return resolve(body);
    		});
		});
	}
}

function getClient() { 
	var server = config.get('server');

	if (server === false) {
		throw new Error( 'You must first set the linkr server URL.\r\n\t\t\t     Example: linkr server http://www.domain.com/' );
	}

	var options = { headers: { 'User-Agent': 'linkr/' + package.version + ' (cli)' } };

	return request.createClient( server, options ); 
}