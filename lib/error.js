//	Global Uncaught Error/Exception/Rejection Handler
var output = require( __dirname + '/output' );

process.on( 'uncaughtException', 	errorOutput );
process.on( 'unhandledRejection', 	errorOutput );

function errorOutput( error ) {
	output.error( error );
	process.exit(1);
}
