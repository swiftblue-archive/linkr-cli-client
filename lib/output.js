var logSymbols  = require('log-symbols');
var CLI         = require('clui');
var clc         = require('cli-color');
var _           = require('lodash');
var config      = require( __dirname + '/config' );
var app         = require( __dirname + '/../package.json' );
var Line       = CLI.Line;
var LineBuffer = CLI.LineBuffer;

var outputWidth = 60;

var output = {
	config: { 
		padding: 10,
		width:   outputWidth,

		header: {
			padding: 0,
			style: [ clc.xterm(240) ]
		},

		footer: {
			padding: 0,
			style: [ clc.xterm(240) ],
		},		

		divider: {
			char:        '─',
			style:       [ clc.xterm(240) ],
			len:         outputWidth,
			padding: 0
		}
	},

	filler: _.repeat( ' ', outputWidth ),

	success: function( title, url ) {
		
		var itemTitle = 'LINK:';

		elements
			.blank()
			.blank()
			.header( title )
			.blank()
			.divider()
			.blank()

			new Line()
				.padding(10)
				.padding( output.config.padding - 5 )
				.column( logSymbols.success, 5, [] )
				.column( itemTitle, itemTitle.length, [clc.yellow])
				.padding( 3 )
				.column( url, url.length, [clc.blueBright] )
				.padding( output.config.padding )
				.fill()
				.output();

		
		elements
			.blank()
			.divider()
			.footer()
			.blank()
			.blank();

		return output;
	},

	error: function( error ) {
		var message    = error.message || error;
		var title      = 'UH OH! THERE IS A PROBLEM!';
		var errorTitle = 'ERROR:';

		elements
			.blank()
			.blank()
			.header( title )
			.blank()
			.divider()
			.blank()

			new Line()
				.padding(10)
				.padding( output.config.padding - 5 )
				.column( logSymbols.error, 5, [] )
				.column( errorTitle, errorTitle.length, [clc.red])
				.padding( 3 )
				.column( message, message.length, [clc.redBright] )
				.padding( output.config.padding )
				.fill()
				.output();

		
		elements
			.blank()
			.divider()
			.footer()
			.blank()
			.blank();

		return output;		
	}
}

module.exports = output;


//
//	Private Helper/Builder Classes
//	


var elements = {
	blank: function() { 
		new Line().fill().output(); 
		return elements;
	},

	fill: function( styles ) {
		new Line()
			.column( output.filler, output.config.width, styles )
			.fill()
			.output();
		return elements;
	},

	header: function( title ) {
	
		var padding = utils.center( title, output.config.width );
		var titleFill = utils.centerFill( title, output.config.width );
		
		// output.fill( output.config.header.style );

		new Line()
		.padding(10)

			.column( _.toUpper(titleFill), output.config.width, output.config.header.style )
			.fill()
			.output();

		// output.fill( output.config.header.style );	

		return elements;		
	},
	
	footer: function() {		
		var version        = 'v' + app.version;
		var appTitleLength = (output.config.width - version.length);

		new Line()
		.padding(10)

			.column( 'Linkr',	appTitleLength,		output.config.footer.style )
			.column( version, 	version.length, 	output.config.footer.style )
			.fill()
			.output();

		return elements;
	},

	divider: function( width ) {
		var width  = width || output.config.width;
		var string = _.repeat( output.config.divider.char, width );

		new Line()
		.padding(10)

			.padding( output.config.divider.padding )
			.column( string, 	width, 	output.config.divider.style )
			.fill()
			.output();

		return elements;
	},
}

var utils = {
	center: function( text, width ) {
		return parseInt( ((width - text.length) / 2).toFixed());
	},

	centerFill: function( text, width ) {
		var padding = parseInt( ((width - text.length) / 2).toFixed());
		var fill    = _.repeat( ' ', padding );
		var result  = fill + text + fill;

		return result;
	}
}