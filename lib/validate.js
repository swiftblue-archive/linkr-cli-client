var validator = require('validator');
var _         = require('lodash');

var validate = {
	url: function( url ) {
		var _isHttp  = _.startsWith( url, 'http://' );
		var _isHttps = _.startsWith( url, 'https://' );

		if (!_isHttp && !_isHttps)		var url = 'http://' + url;
		if (!_.endsWith( url, '/' )) 	var url = url + '/';

		var isValidUrl = validator.isURL( url, { protocols: [ 'http', 'https' ] } );
		if (!isValidUrl) throw new Error( 'The server value must be a valid URL. Example: http://www.domain.com/' );

		return url;
	}
}

module.exports = validate;