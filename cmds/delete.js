'use strict';
var controllers = require( __dirname + '/../controllers' );

module.exports = function(program) {

	program
		.command( 'delete [slug]' )
		.description('Delete a link')
		.action( controllers.link.delete );

};
