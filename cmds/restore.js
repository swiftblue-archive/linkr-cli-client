'use strict';
var controllers = require( __dirname + '/../controllers' );

module.exports = function(program) {

	program
		.command( 'restore <slug>' )
		.description( 'Restore a deleted link' )
		.action( controllers.link.restore );

};
