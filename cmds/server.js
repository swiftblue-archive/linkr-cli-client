'use strict';
var controllers = require( __dirname + '/../controllers' );

module.exports  = function(program) {

	program
		.command( 'server [url]' )
		.description('Set linkr backend server [url] or omit [url] to see current setting')
		.action( controllers.server );

};