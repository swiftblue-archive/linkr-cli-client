'use strict';
var controllers = require( __dirname + '/../controllers' );

module.exports = function(program) {

	program
		.command( 'create <URL> [slug]' )
		.description('Create a new link')
		.action( controllers.link.create );

};
