'use strict';
var controllers = require( __dirname + '/../controllers' );

module.exports = function(program) {

	program
		.command( 'info <slug>' )
		.description( 'Get info for a link')
		.action( controllers.link.info );

};
